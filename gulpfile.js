/**
 * @var gulp
 * require - gulp proper
 */
var gulp = require('gulp');

/**
 * @var sass
 * require - gulp-sass plugin
 */
var sass = require('gulp-sass');

/**
 * @var browserSync
 * require - browser sync (reload browser on file changes)
 */
var browserSync = require('browser-sync').create();

/**
 * @var cache
 * require - gulp-cache for caching
 */
var cache = require('gulp-cache');

/**
 * @var runSequence
 * require - runSequence
 */
var runSequence = require('run-sequence');

gulp.task('default', ['sass', 'resizeSensor']);

// Task - initiate browserSync to reload browser on file changes.
gulp.task('browserSync', function() {
  browserSync.init({
    proxy: "rachaelrayshow.test"
  });
});

// Task - compile sass.
gulp.task('sass', function(){
  return gulp.src('scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// Task - copy ResizeSensor js to theme folder.
gulp.task('resizeSensor', function(){
  return gulp.src('node_modules/css-element-queries/src/ResizeSensor.js')
    .pipe(gulp.dest('js'))
});

// Task - watch for file changes
gulp.task('watch', ['browserSync', 'sass'], function(){
  // Reloads the browser whenever sass changes.
  gulp.watch('scss/**/*.scss', ['sass']);

  // Reloads the browser whenever Twig or JS files change.
  gulp.watch('templates/**/*.twig', browserSync.reload);
  gulp.watch('js/**/*.js', browserSync.reload);
});

